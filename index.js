const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose')
const {authRouter} = require('./routes/authRoute')
const  {userRouter} = require('./routes/userRoute')
const {truckRouter} =require('./routes/truckRoute')
const {loadRouter}  = require('./routes/loadRoute')

const app = express();

app.use(express.json());
app.use(morgan("tiny"));

//routes
app.use("/api/auth", authRouter);
app.use("/api/users", userRouter);
app.use("/api/trucks", truckRouter);
app.use("/api/loads", loadRouter);

async function Start (){
    try{
        await mongoose.connect(`mongodb+srv://Valery:V.nduY8W$izHn.u@cluster0.gjrdk.mongodb.net/HT3?retryWrites=true&w=majority`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        app.listen(8080);
        console.log("DB connected")
    }catch (e)
    {
        console.log("DB not connected");
    }
}

Start() ;

