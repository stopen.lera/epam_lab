const jsontoken = require("jsonwebtoken");

const tokenVerification = (authorization) => {
    if (!authorization) {
        return null;
    }
    const [, token] = authorization.split(" ");
    if (!token) {
        return null;
    }
    try {
        const tokenPayload = jsontoken.verify(token, "key");
        const User = {
            _id: tokenPayload._id,
            email: tokenPayload.email,
            created_date: tokenPayload.created_date,
            role: tokenPayload.role,
        };
        return User;
    } catch (err) {
       return null;
    }
};
module.exports = {
    tokenVerification,
};
