const {register, logIn, forgotPass} = require('../services/authService')
const express = require("express")


const authRouter = express.Router();

authRouter.post("/register", async (req, res, next)=>{
    try{
        const {email, password, role} = req.body;
        if(email && password && role) {
            const result = await register(email, password, role);
            if (result == null) {
                res.status(400).json({message: `User with email: ${email} already exists`})
            } else {
                res.status(200).json({message: "Profile created successfully"});
            }
        }
        else{
            res.status(400).json({message: "Client error"});
        }
    }
    catch (e)
    {
        res.status(500).json({message:`${e.message}`});
    }
})

authRouter.post("/login", async (req, res, next)=>{
    try {
        const {email, password} = req.body;
        if (email && password) {
            const token = await logIn(email, password);
            if (!token) {
                res.status(400).json({message: "Wrong email or password"});
            } else {
                res.status(200).json({"jwt_token": token});
            }
        }
        else{
            res.status(400).json({message: "No email or password"});
        }
    }
    catch(e){
        res.status(500).json({message:`${e.message}`});
    }
})
 authRouter.post("/forgot_password", async (req, res, next)=>{
     try{
         const {email} = req.body;
         if(email){
             const result = await forgotPass(email);
             if(!result){
                 res.status(400).json({message: "Wrong email"});
             }
             else{
                 res.status(200).json({message: "New password sent to your email address"});
             }
         }
         else{
             res.status(400).json({message: "No email"});
         }
     }catch (e) {
         res.status(500).json({message:`${e.message}`});
     }
})

module.exports = {authRouter};

