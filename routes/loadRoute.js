const express = require("express")
const {
    addLoad,
    getLoads,
    getActiveLoad,
    changeState,
    getLoadById,
    editLoad,
    deleteLoad,
    findDriver,
    getInfo
} = require("../services/loadService")

const loadRouter = express.Router();
loadRouter.post("/", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
        const load = await addLoad(authorization, name, payload, pickup_address, delivery_address, dimensions);
        if (!load) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: "Load created successfully"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.get("/", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        let offset = 0;
        let limit = 0;
        if (req.query.offset && req.query.limit) {
            offset = parseInt(req.query.offset);
            limit = parseInt(req.query.limit);
        }
        const loads = await getLoads(authorization, offset, limit);
        if (!loads) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({loads: loads});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.get("/active", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        const Load = await getActiveLoad(authorization);
        if(Load) {
            res.status(200).json({load: Load});
        }
        else{
            res.status(400).json({message: "Client error"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.patch("/active/state", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        const changed = await changeState(authorization);
        if (!changed) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: `Load state changed to ${changed}`});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.get("/:_id", async (req, res, next) => {
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const Load = await getLoadById(authorization, _id);
        if (!Load) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(400).json({load: Load})
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.put("/:_id", async (req, res, next) => {
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
        const edit = await editLoad(authorization, _id, name, payload, pickup_address, delivery_address, dimensions)
        if (!edit) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(400).json({message: "Load details changed successfully"})
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.delete("/:_id", async (req, res, next) => {
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const del = await deleteLoad(authorization, _id);
        if (!del) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(400).json({message: "Load deleted successfully"})
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.post("/:_id/post", async (req, res, next) => {
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const find = findDriver(authorization, _id)
        if (find == null) {
            res.status(400).json({message: "Client error"})
        } else if (find == false) {
            res.status(200).json({message: "Load not posted", driver_found: false})
        } else {
            res.status(200).json({message: "Load posted successfully", driver_found: true})
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

loadRouter.get("/:_id/shipping_info", async (req, res, next) => {
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const {Load, Truck} = await getInfo(authorization, _id)
        console.log(Load)
        console.log(Truck)
        if(!Load || !Truck){
            res.status(400).json({message: "Client error"})
        }else{
            res.status(200).json({load: Load, truck: Truck})
            res.status(200).json({load: Load, truck: Truck})
        }

    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

module.exports = {loadRouter}
