const express = require("express")
const {addTruck,  getTrucks, getTruck, putTruck, assignTruck, deleteTruck} = require("../services/truckService")

const truckRouter = express.Router();

truckRouter.post("/",async(req, res, next)=>{
    try {
        const authorization = req.headers.authorization;
        const {type} = req.body;
        const truck = await addTruck(authorization, type);
        if (!truck) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: "Truck created successfully"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

truckRouter.get("/",async(req, res, next)=>{
    try {
        const authorization = req.headers.authorization;
        const trucks = await getTrucks(authorization);
        if (!trucks) {
            res.status(400).json({message: "Client errorn"})
        } else {
            res.status(200).json({trucks: trucks});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

truckRouter.get("/:_id",async(req, res, next)=>{
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const Truck = await getTruck(authorization, _id);
        if (!Truck) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({truck: Truck});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

truckRouter.put("/:_id",async(req, res, next)=>{
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const {type} = req.body;
        const truck = await putTruck(authorization, type, _id);
        if (!truck) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: "Truck details changed successfully"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})
truckRouter.post("/:_id/assign",async(req, res, next)=>{
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const truck = await assignTruck(authorization, _id);
        if (!truck) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: "Truck assigned successfully"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

truckRouter.delete("/:_id",async(req, res, next)=>{
    try {
        const {_id} = req.params;
        const authorization = req.headers.authorization;
        const truck = await deleteTruck(authorization, _id);
        if (!truck) {
            res.status(400).json({message: "Client error"})
        } else {
            res.status(200).json({message: "Truck deleted successfully"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

module.exports = {truckRouter}
