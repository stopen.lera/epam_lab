const express = require("express")
const {getUser, patchUser, deleteUser} = require("../services/userService")

const userRouter = express.Router();

userRouter.get("/me", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        const User = await getUser(authorization);
        if (!User) {
            res.status(400).json({message: "Wrong token"})
        } else {
            res.status(200).json({"user": User});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

userRouter.patch("/me/password", async (req, res, next) => {
    try {
        const {oldPassword, newPassword} = req.body;
        if (oldPassword && newPassword) {
            const authorization = req.headers.authorization;
            const pass = await patchUser(authorization, oldPassword, newPassword);
            if (!pass) {

                res.status(200).json({message: "Password changed successfully"});
            } else {
                res.status(400).json({message: "Wrong password or token"})
            }
        } else {
            res.status(400).json({message: "Client error"});
        }
    } catch (e) {
        res.status(500).json({message: `${e.message}`});
    }
})

userRouter.delete("/me", async (req, res, next) => {
    try {
        const authorization = req.headers.authorization;
        const del = await deleteUser(authorization);
        if (del != null) {
            res.status(200).json({message: "Profile deleted successfully"});
        } else {
            res.status(400).json({message: "Client error"});
        }

    } catch (e) {
        res.status(500).json({message: "Server error"});
    }
})


module.exports = {userRouter}


