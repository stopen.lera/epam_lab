const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false);

const userSchema = new mongoose.Schema({
    role: {type:String, required:true, enum:["DRIVER", "SHIPPER"]},
    email: {type: String, required: true, unique: true,},
    password: {type: String, required: true,},
    created_date: {type: Date, default: Date.now(),}
});

const userModel = mongoose.model(`users`, userSchema);

module.exports = {userModel};
