const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false);

const truckSchema = new mongoose.Schema({
    created_by: {type:String, required:true},
    assigned_to: {type: String, default: null},
    type: {type: String, required: true, enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"]},
    status: {type: String, required: true, enum:["OL", "IS"],  default: "IS"},
    created_date: {type: Date, default: Date.now(),}
});

const truckModel = mongoose.model(`trucks`, truckSchema);

module.exports = {truckModel};
