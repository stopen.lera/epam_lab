const bcrypt = require ('bcrypt');
const{tokenVerification} = require('../tokenVerification')
const {userModel} = require ('../models/userModel');

const getUser = async (authorization)=>{
    try{
        const User = await tokenVerification(authorization);
        if(!User){
            throw new Error("Wrong token")
        }
        const User2 = await userModel.findOne({email: User.email});
        if(!User2){
            throw new Error("Wrong token")
        }
        return User;
    }catch (e){
        console.log(e.message);
        return null;
    }
}

const patchUser = async (authorization, old_pass, new_pass)=>{
    try {
        const User = await getUser(authorization);
        if(!User){
            throw new Error("Wrong token")
        }
        const User2 = await userModel.findOne({email: User.email});
        const isValid = await bcrypt.compare(old_pass, User2.password);
        if (isValid && new_pass) {
            await userModel.findOneAndUpdate({email: User.email}, {password: await bcrypt.hash(new_pass, 10)})
            return 1;
        }
        else {
            throw new Error("Wrong password")
        }
    }
    catch (e){
        console.log(e.message);
        return null;
    }
}

const deleteUser = async (authorization)=>{
    try
    {
        const User = await getUser(authorization);
        if(!User){
            return  null;
        }
        await userModel.deleteOne({email: User.email});
        return 1;

    }catch (e){
        console.log(e.message);
        return null;
    }
}

module.exports = {getUser, patchUser, deleteUser}

