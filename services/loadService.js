const {loadModel} = require('../models/loadModel');
const {getUser} = require('./userService');
const {truckModel} = require('../models/truckModel')

const addLoad = async (authorization, name, payload, pickup_address, delivery_address, dimensions) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "SHIPPER") {
            throw new Error("No access rights")
        }
        const Load = new loadModel({
            created_by: User._id,
            name: name,
            payload: payload,
            pickup_address: pickup_address,
            delivery_address: delivery_address,
            dimensions: dimensions,
        });
        await Load.save();
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getLoads = async (authorization, offset, limit) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        let loads = []
        if (User.role === "SHIPPER") {
            loads = await loadModel.find({created_by: User._id});
        } else if (User.role === "DRIVER") {
            loads = await loadModel.find({assigned_to: User._id});
        }
        if (offset > -1 && limit > 0) {
            return await loads.slice(0 + offset, limit + offset)
        } else {
            return loads
        }
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getActiveLoad = async (authorization) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        let Load = await loadModel.findOne({});
        Load.assigned_to = User._id
        Load.status = "ASSIGNED"
        Load.state="En route to Pick Up"
        return Load;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const changeState = async (authorization) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        let Load = await loadModel.findOne({assigned_to: User._id});
        if (!Load) {
            throw new Error("No active load")
        }
        if (Load.state === "En route to Pick Up") {
            await loadModel.findOneAndUpdate({assigned_to: User._id}, {state: "Arrived to Pick Up"});
            return "Arrived to Pick Up";
        }
        if (Load.state === "Arrived to Pick Up") {
            await loadModel.findOneAndUpdate({assigned_to: User._id}, {state: "En route to delivery"});
            return "En route to delivery";
        }
        if (Load.state === "En route to delivery") {
            await loadModel.findOneAndUpdate({assigned_to: User._id}, {state: "Arrived to delivery"});
            Load.status = "SHIPPED"
            await Load.save()
            let Truck = await truckModel.findOne({assigned_to: Load.assigned_to})
            Truck.status = "IS"
            await Truck.save()
            return "Arrived to delivery";
        }
        throw new Error("Wrong state")
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getLoadById = async (authorization, _id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        if (User.role === "DRIVER") {
            const Load = await loadModel.findOne({assigned_to: User._id, _id: _id});
            return Load
        }
        if (User.role === "SHIPPER") {
            const Load = await loadModel.findOne({created_by: User._id, _id: _id});
            return Load
        }
        return null;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const editLoad = async (authorization, _id, name, payload, pickup_address, delivery_address, dimensions) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        if (User.role !== "SHIPPER") {
            throw new Error("No access rights")
        }
        const Load = await loadModel.findOne({created_by: User._id, _id: _id, status:"NEW"});
        if (!Load) {
            throw new Error("No load")
        }
        await loadModel.findOneAndUpdate({_id: _id}, {
            name: name,
            payload: payload,
            pickup_address: pickup_address,
            delivery_address: delivery_address,
            dimensions: dimensions
        });
        return 1;

    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const deleteLoad = async (authorization, _id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        if (User.role !== "SHIPPER") {
            throw new Error("No access rights")
        }
        const Load = await loadModel.findOne({created_by: User._id, _id: _id, status:"NEW"});
        if (!Load) {
            throw new Error("Wrong id")
        }
        await loadModel.deleteOne({_id: id});
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const findDriver = async (authorization, _id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        if (User.role !== "SHIPPER") {
            throw new Error("No access rights")
        }
        let Load = await loadModel.findOne({created_by: User._id, _id: _id});
        if (Load.status !== "NEW") {
            throw new Error("Wrong status")
        }
        Load.status = "POSTED";
        let trucks = []
        trucks = await truckModel.find({status: "IS"})
        if (!trucks) {
            return false
        }
        let Truck = {}
        for(i = 0; i<trucks.length; i++){
            if (trucks[i].assigned_to) {
                let Tr = trucks[i]
                if (Tr.type==="SPRINTER"){
                    if(Load.dimensions.width<300 && Load.dimensions.length<250 && Load.dimensions.height<170 && Load.payload<1700){
                        Truck = Tr
                        break
                    }
                }
                else if(Tr.type==="SMALL STRAIGHT"){
                    if(Load.dimensions.width<500 && Load.dimensions.length<250 && Load.dimensions.height<170 && Load.payload<2500){
                        Truck = Tr
                        break
                    }
                }
                else if(Tr.type==="LARGE STRAIGHT"){
                    if(Load.dimensions.width<700 && Load.dimensions.length<350 && Load.dimensions.height<200 && Load.payload<400){
                        Truck = Tr
                        break
                    }
                }
            }
        }
        console.log(Truck)
        if (!Truck) {
            return false
        }
        Load.assigned_to = Truck.assigned_to
        Load.status = "ASSIGNED"
        Load.state = "En route to Pick Up"
        await Load.save()
        Truck.status = "OL"
        await Truck.save()
        return true

    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getInfo = async(authorization, _id)=>{
    try{
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        }
        if (User.role !== "SHIPPER") {
            throw new Error("No access rights")
        }
        let Load = await loadModel.findOne({created_by: User._id, _id: _id});
        if (!Load) {
            throw new Error("Wrong ID")
        }
        let Truck =await truckModel.findOne({assigned_to: Load.assigned_to})
        if(!Truck){
            throw new Error("Load is not active")
        }
        return {Load, Truck}
    }catch (e) {
        console.log(e.message);
        return null;
    }
}

module.exports = {addLoad, getLoads, getActiveLoad, changeState, getLoadById, editLoad, deleteLoad, findDriver, getInfo}

