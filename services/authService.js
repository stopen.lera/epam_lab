const {userModel} = require('../models/userModel');
const bcrypt = require('bcrypt');
const jsontoken = require('jsonwebtoken');

const register = async (email, password, role) => {
    try {
        if (await userModel.findOne({email: email})) {
            throw new Error("Email already exists")
        }
        const User = new userModel({email: email, password: await bcrypt.hash(password, 10), role: role});
        await User.save();


        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}
const logIn = async (email, password) => {
    try {
        const User = await userModel.findOne({email: email});
        if (!User) {
            throw new Error("Wrong username");
        }
        const isValid = await bcrypt.compare(password, User.password);
        if (!isValid) {
            throw new Error("Wrong password");
        }
        return jsontoken.sign({_id: User._id, email: email, created_date: User.created_date, role: User.role}, "key");
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const forgotPass = async (email) => {
    try {
        const User = await userModel.findOne({email: email});
        if (!User) {
            throw new Error("Wrong username");
        }
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

module.exports = {register, logIn, forgotPass};
