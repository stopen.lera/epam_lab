const {truckModel} = require('../models/truckModel');
const {getUser} = require('./userService');

const addTruck = async (authorization, type) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const Truck = new truckModel({created_by: User._id, type: type});
        await Truck.save();
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getTrucks = async (authorization) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const trucks = await truckModel.find({created_by: User._id});
        return trucks;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const getTruck = async (authorization, id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const Truck = await truckModel.findOne({created_by: User._id, _id: id});
        return Truck;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const putTruck = async (authorization, type, id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const Truck = await truckModel.findOne({created_by: User._id, _id: id});
        if (!Truck) {
            throw new Error("Wrong id")
        }
        await truckModel.findOneAndUpdate({_id: id}, {type: type});
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const assignTruck = async (authorization, id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const Truck = await truckModel.findOne({created_by: User._id, _id: id});
        if (!Truck) {
            throw new Error("Wrong id")
        }
        await truckModel.findOneAndUpdate({_id: id}, {assigned_to: User._id});
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

const deleteTruck = async (authorization, id) => {
    try {
        const User = await getUser(authorization);
        if (!User) {
            throw new Error("Wrong token")
        } else if (User.role !== "DRIVER") {
            throw new Error("No access rights")
        }
        const Truck = await truckModel.findOne({created_by: User._id, _id: id});
        if (!Truck) {
            throw new Error("Wrong id")
        }
        if (Truck.status !== "IS") {
            throw new Error("Truck is on road")
        }
        await truckModel.deleteOne({_id: id});
        return 1;
    } catch (e) {
        console.log(e.message);
        return null;
    }
}

module.exports = {addTruck, getTrucks, getTruck, putTruck, assignTruck, deleteTruck}
